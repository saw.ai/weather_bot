import config
import utils
import telebot
import requests
import random


from subprocess import call, Popen, PIPE, STDOUT


def download_image(url):
    call(['wget', url, '-O', 'photo'])



def generate_png(sentence):

    city, translation = utils.get_city_name(sentence)

    if city == None:
        return False, None, None
    weather = utils.get_weather(city.lower())

    delta = 0
    if 'сегодня' in sentence:
        delta = 0
    elif 'завтра' in sentence:
        delta = 1
    elif 'послезавтра' in sentence:
        delta = 2
    elif utils.day_of_week_in(translation) != None:
        delta = utils.get_delta_week(translation)

    current = weather['daily']['data'][delta]

    month, day = utils.get_date_after(delta)

    city_russian = utils.get_city_russian(city)

    svg = utils.make_card(city=city_russian,
                          day_of_week=utils.get_week_name_by_delta(delta),
                          day_of_month=day,
                          month=month,
                          summary=current['summary'],
                          max_t=utils.to_celciy(current['temperatureMax']),
                          min_t=utils.to_celciy(current['temperatureMin']),
                          icon=current['icon'])


    def svg_to_png(svg):
        converter = Popen([config.inkspace, '/dev/stdin', '-e', '/dev/stderr'],
                          stdin=PIPE, stdout=PIPE, stderr=PIPE)

        output, errors = converter.communicate(input=str.encode(svg))

        png_begin = errors.find(b'\x89PNG')
        png = errors[png_begin:]

        return png

    png = svg_to_png(svg)

    with open('card.png', 'wb') as f:
        f.write(png)
        f.close()

    return True, city, current['icon']


def get_st(icon):
    s = None
    if 'cloudy' in icon:
        s = 'cloudy'
    elif 'wind' in icon:
        s = 'wind'
    elif 'rain' in icon:
        s = 'rain'
    elif 'snow' in icon:
        s = 'snow'
    else:
        s = 'clear'

    texts = open('st').read().split('===' + s)[1].split('===')[0].split('+++')
    text = random.choice(texts)
    return text




def get_image(city):
    url = 'https://duckduckgo.com/?q={} фото&t=hg&iar=images'.format(city)
    response = requests.get(url)
    vqd = response.text.split('vqd=\'')[1].split('\'')[0]


    url = 'https://duckduckgo.com/i.js?l=ru-ru&o=json&q={} фото&vqd={}&f=,,,&p=1'.format(city, vqd)
    response = requests.get(url)

    data = response.json()
    url_image = data['results'][0]['image']
    download_image(url_image)


bot = telebot.TeleBot(config.token)


@bot.message_handler(content_types=["text"])
def repeat_all_messages(message):
    # bot.send_message(message.chat.id, message.text)

    if message.text in ['/help', '/start']:
        bot.send_message(message.chat.id, 'Укажите город, в котором вы хотите узнать погоду. Также можно указать день недели или ближайшее будущее (сегодня, завтра, послезавтра). Например, "Какая погода будет во вторник в Севастополе?"')
    else:
        ok, city, icon = generate_png(message.text)
        if ok:
            img = open('card.png', 'rb')
            bot.send_chat_action(message.chat.id, 'upload_photo')
            bot.send_photo(message.chat.id, img)
            img.close()

            get_image(city)
            img = open('photo', 'rb')
            bot.send_chat_action(message.chat.id, 'upload_photo')
            bot.send_photo(message.chat.id, img)
            img.close()

            mess = get_st(icon)
            bot.send_message(message.chat.id, mess)

        else:
            bot.send_message(message.chat.id, 'Извините, города не найдено')





if __name__ == '__main__':
    bot.polling(none_stop=True)


