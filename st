===rain
Где дождь, где сад - не различить.
Весь сад в дожде! Весь дождь в саду!
Погибнут дождь и сад друг в друге,
Как разниму я сад и дождь
Все дождь и сад сведут на нет,
под ивовый дождь ее частых отверстий!
Дождь веков нас омыл и промаслил.
Со мной с утра не расставался Дождь.
Дождь, как крыло, прирос к моей спине.
Дождь был со мной, забыв про все на свете.
Дождь за окном пристроился, как нищий,
но тут же Дождь, в печали и отваге,
Дождь на моем плече, как обезьяна,
Дождь на меня смотрел, как сирота.
Прощенный Дождь запрыгал впереди.
Дождь, притаившись за моей спиной,
Я извинилась: - Этот Дождь со мной.
Сквозь дождь! И расстоянья отдаленность! -
За все! За дождь! За после! За тогда!
+++
светится бледною лампой дождь --
куда уходит дождь,
ресниц -- весенний дождь волосяной
дождь в пространстве стоит как слеза,
"особливо, ежели, скажем, дождь или сухо".
Дождь пахнет мутной оторопью окон
попасть под майский дождь в Москве
+++
Как дождь весной - листве лесной,
Тебе ни дождь, ни снег, ни град
То дождь, то снег хлестал в окно,
Кропя, как дождь, в полдневный зной
В дождь и стужу, в бурю и туман,
Дробный дождь, трескучий град.
+++
январский дождь целует в щеку.
ГРИБНОЙ ДОЖДЬ НА КЕЙП КОДЕ
что дождь поцелуй горловой и губной.
Там в Орегоне ты прячешь дождь
Гляди: за шиворот реки стекает дождь.

==snow
как будто выпал снег и стаял.
над черным снегом нависая,
и, придавая очертанья снегу,
приподнимаем белый снег с земли.
Снег уточняет все свои черты
лицо, что к снегу обращаешь ты.
снегопада, обрыва, куста.
пред красою деревьев в снегу,
под снегом, вырастающим на кровле,
Он слишком юн, чтоб предаваться снегу.
лишила юг опалы снегопада.
чтоб тенью снег не утруждать.
по снегу белому иду.
не говорю: тебя - как свет! как снег! -
+++
Снегопад свое действие начал
и ликующий лыжник снегов.
Но, пока в снегопаданье строгом
весь снег вселенной, всю луну.
И в снегопаде, долго бывшем,
+++
и розовый снег кругом,
я припомню какой в этом городе снег --
какой мягкий снег, какой розовый снег,
В белом снегу -- в бороде патриарха --
Наверное, будет снег,
приблизятся сквозь снег и улыбнутся.
хранящие убитый снег дворы.
За марлевой завесой снегопада,
беспризорный вьется снег,
кленов, дома -- там снега...
на котором тает глубокий снег, превращается в свет лед,
пусть останется серое мессиво снега,
это -- снег -- не мой, сыпучий-скрипучий дом.
и капает прозрачный снег,
Дней обраставших листвой и снегом
Потом снегопад одуряющий, жаркий,
Наверно скучаю. Вижу -- ты... и идет снег.
не падал снег
на всем теперь, как снег
по снегу в выдышанный отсылает воздух.
+++
Прощайте, вершины под кровлей снегов,
Теперь ты снегом убелен, -
Свежий, чистый, точно снег.
Когда глубокий снег зимою
Тебе ни дождь, ни снег, ни град
Как снег, сияли белизной
В снегах на утесе крутом!
Завалит снегом нашу дверь, -
То дождь, то снег хлестал в окно,
Торговец утонул в снегу.
Пускай припомнит град, и снег,
Январский ветер, колкий снег.
В полях, под снегом и дождем,
На вершинах, снегопад!
Ни морозов, ни снегов, ни инея.
А грудь, как первый снег, бела.
В грозу и ливень, в снег и зной.
"В полях под снегом и дождем..." 
- Впервые под названием "Тебе одной" в
+++
Сыплет черемуха снегом,
Сыпь ты, черемуха, снегом,
Принакрылась снегом,
Под копытом на снегу,
Валит снег и стелет шаль.

===cloudy
Вдруг облаком тебя покроет,
Откуда облако взялось?
а нежность-облаком вчерашним,
лицом ее, облаком неочевидным,
серебряное облако детей.
В дверь ваших гор и облаков
Морским облаком и небесами,
что был я прежде? Облако? Звезда?
Мне стать хотелось облаком, звездою,
черное облако, смерти слепая всеядность.
Только облако в небе. Да эхо.
Лишь одно только облако это, -
вялое облако втиснулось в эту ловушку.
Пуст и свободен, я облаком шел к облакам,
+++
падает облако вниз
будет только абрис облаков
Позади пыль серая, как облако

===wind
Уже рассветный ветер дунул,
в что за ветер в эту ночь запущен?
Играет ветер в тени, в голоса,
был ветер пьяным от вина.
О ветер,
и ветер меня гладит по плечам.
А ветер - он буян и соглядатай,
толкают руки ветерка.
страшны ли тебе ветер и гроза?
страшны ли тебе ветер и гроза?!
Мне повадно и в стужу и в ветер
Даже ветер с другой стороны!
сколько раз ветерок этот дунул,
и встречный ветер бьет, и в пустырях
+++
вдыхающая млечный ветер.
космы бросив в ветер --
ибо ветер терзает их, озверев,
в стылый чухонский ветер --
Подгоняет волну неживой ветерок
и вещий ветер ровно дует,
Уже ветерок нагловатый землицей сырою пропах --
как летний ветер полный светляками.
+++
Как ветер с градом и дождем
Зимний ветер шумел, низко тучи плыли.
Пусть ветер, воя, точно зверь,
Чтоб ветер счастья пеной пьяной
И прочь летят, как ветерок.
Дул ветер из последних сил,
Январский ветер, колкий снег.
О ветер западный, повей,
И ветер гонит облака,
Даришь, как глупый ветерок,
Западный ветер. - Впервые без названия в книге "Роберт Бернс", 1950.
Как вешний ветер, что в пути
+++
Кудри черные змейно трепал ветерок.
Схимник ветер шагом осторожным
Пляшет ветер по равнинам

===clear
Прост путь к свободе, к ясности ума -
с какой-то ясною печалью.
пред ясной и бесхитростной свечою,
ты слышишь в ясном небе гром
+++
Вечерами за нашими спинами топчется ясность.
Наступает атасное утро после ясной ночи.
те что мерещатся утром неясно, едва,
она уводит вглубь воды неясной,
неясно отражают наблюденья,
изображают ясно: он смущен,
+++
И даль долин, и ясность вод
Вот, наконец, неясной тенью
И видят в ясной глубине

===
