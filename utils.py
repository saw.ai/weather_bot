import requests
import json
import mysql.connector

import datetime

wdays = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота',
         'воскресенье']
wdays_en = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday',
            'sunday']

def day_of_week_in(sentence):
    for i in range(len(wdays_en)):
        if wdays_en[i] in sentence.lower():
            return i

    return None

def get_delta_week(sentence):
    wday = day_of_week_in(sentence)
    now = datetime.datetime.today().weekday()

    return (wday + 7 - now) % 7

def get_week_name_by_delta(delta):
    now = datetime.datetime.today().weekday()
    name = wdays[(now + delta) % 7]
    name = name[0].upper() + name[1:]
    return name

def get_date_after(delta):
    months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    date = datetime.date.today() + datetime.timedelta(days=delta)
    return months[date.month-1], date.day


def to_celciy(x):
    return int((x - 32) * 5 / 9)

ifile = open('worldcitiespop.txt', 'rb')
data = ifile.read().decode('utf-8', 'ignore')
ifile.close()
lines = data.split('\n')
cities = set()
for line in lines:
    if line.count(',') > 2:
        cities.add(line.split(',')[1])

def check_the_name(candidate):
    if candidate.lower() in ['weather']:
        return False
    return candidate.lower() in cities


def get_city_name(sentence):

    key = 'trnsl.1.1.20171121T053147Z.d8fa427abf331155.4b8b909129ed078af5193c2a95894014ae1a8f33'
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key={}&text={}&lang=ru-en'.format(key, sentence)

    response = requests.get(url)
    result = response.json()
    words = result['text'][0].split(' ')

    candidates = []

    for i in range(1, len(words)):
        candidate = words[i-1] + ' ' + words[i]
        if check_the_name(candidate):
            candidates.append(candidate)

    for word in words:
        if check_the_name(word):
            candidates.append(word)

    candidates = sorted(candidates, key=lambda x: len(x), reverse=True)

    if len(candidates) > 0:
        return candidates[0], result['text'][0]

    return None, None


def get_city_russian(city):

    key = 'trnsl.1.1.20171121T053147Z.d8fa427abf331155.4b8b909129ed078af5193c2a95894014ae1a8f33'
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key={}&text={}&lang=en-ru'.format(key, city)

    response = requests.get(url)
    result = response.json()
    return result['text'][0]


def get_weather(city):
    url = 'https://duckduckgo.com/js/spice/forecast/{}%20{}/ru'.format(city, city)
    response = requests.get(url)
    begin = response.text.find('ddg_spice_forecast(') + len('ddg_spice_forecast(')
    data = response.text[begin: -2]
    result = json.loads(data)
    return result



def make_card(city, day_of_week, day_of_month, month, summary, max_t, min_t, icon):

    icon_path = None
    try:
        icon_path = open('icons/' + icon + '.svg').read()
    except:
        icon_path = open('icons/rain.svg').read()

    icon_path = icon_path[icon_path.find('<path'):]
    icon_path = icon_path[:icon_path.find('>')]

    card = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="800"
   height="800"
   viewBox="0 0 0 1052.3622"
   id="svg2"
   version="1.1"
   inkscape:version="0.91 r13725"
   sodipodi:docname="card.svg">
  <defs
     id="defs4" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="0.49497475"
     inkscape:cx="555.85536"
     inkscape:cy="358.37559"
     inkscape:document-units="px"
     inkscape:current-layer="layer1"
     showgrid="false"
     units="px"
     inkscape:window-width="1280"
     inkscape:window-height="752"
     inkscape:window-x="0"
     inkscape:window-y="0"
     inkscape:window-maximized="1" />
  <metadata
     id="metadata7">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0,-252.36216)">
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="58.588848"
       y="333.13354"
       id="text3336"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3338"
         x="58.588848"
         y="333.13354" /></text>
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:57.82703018px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#1a1a1a;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="424.17703"
       y="439.69028"
       id="text3340"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3342"
         x="424.17703"
         y="439.69028">{} {}</tspan></text>
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#666666;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="428.30469"
       y="498.79858"
       id="text3344"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3346"
         x="428.30469"
         y="498.79858">{}</tspan></text>
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:80.06699371px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="673.75024"
       y="914.41412"
       id="text3348"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3350"
         x="673.75024"
         y="914.41412">{}</tspan><tspan
         sodipodi:role="line"
         x="673.75024"
         y="964.41412"
         id="tspan3352" /></text>
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:83.49629211px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#808080;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;"
       x="675.46686"
       y="1011.3401"
       id="text3354"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3356"
         x="675.46686"
         y="1011.3401">{}</tspan></text>
    <flowRoot
       xml:space="preserve"
       id="flowRoot3362"
       style="font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       transform="translate(-6.0609153,333.17437)"><flowRegion
         id="flowRegion3364"><rect
           id="rect3366"
           width="575.78687"
           height="398.00015"
           x="34.345184"
           y="204.00999" /></flowRegion><flowPara
         id="flowPara3368"
         style="fill:#808080">{}</flowPara></flowRoot>    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="187.88837"
       y="957.40784"
       id="text3370"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3372"
         x="187.88837"
         y="957.40784" /></text>
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:80.95270538px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="26.912043"
       y="343.71396"
       id="text3355"
       sodipodi:linespacing="125%"
       transform="scale(0.98257335,1.0177357)"><tspan
         sodipodi:role="line"
         id="tspan3357"
         x="26.912043"
         y="343.71396">{}</tspan></text>
  </g>
  <g
     id="g24"
     transform="matrix(7.7727629,0,0,7.641466,90.81891,434.9229)">
    <g
       id="g26">
      {}
    </g>
  </g>
</svg>
""".format(month, day_of_month, day_of_week, max_t, min_t, summary, city, icon_path)

    return card


